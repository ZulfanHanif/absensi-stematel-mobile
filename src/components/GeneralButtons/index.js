import { Text, Pressable } from 'react-native';
import React from 'react';
import styles from './styles';

export default function GeneralButtons({ text, onPress, }) {
    return (
        <Pressable
            onPress={onPress}
            style={styles.container}
        >
            <Text
                style={styles.text}
            >
                {text}
            </Text>
        </Pressable>
    )
}