import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
      width: '100%',

      marginTop: 10,
      padding: 12,
      marginVertical: 5,

      alignItems: 'center',
      borderRadius: 15,

      backgroundColor: '#ff0000'
    },
    text: {
      fontSize: 15,
      color: '#fff'
    }
});

export default styles