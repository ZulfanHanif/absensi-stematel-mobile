import { View, Text, TextInput } from 'react-native'
import React from 'react'

import { Controller } from 'react-hook-form';

import styles from './styles'

export default function InputField({
    control,
    name,
    rules = {},
    placeholder,
    secureTextEntry
}) {
    return (
        <Controller
            control={control}
            name={name}
            rules={rules}
            render={({ field: { value, onChange, onBlur }, fieldState: { error } }) => (
                <>
                    <View style={[
                        styles.container,
                        { borderColor: error ? 'red' : '#FF0000' }
                    ]}>
                        <TextInput
                            value={value}
                            onChangeText={onChange}
                            onBlur={onBlur}
                            placeholder={placeholder}
                            style={styles.input}
                            secureTextEntry={secureTextEntry}
                        />
                    </View>
                    {error && (
                        <Text style={{ color: 'red', alignSelf: 'stretch' }}>{error.message || 'Error'}</Text>
                    )}
                </>
            )}
        />
    )
}