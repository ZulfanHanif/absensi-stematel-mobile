import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 20,
      },
      logo: {
        marginTop: 50,
        width: "100%",
        maxWidth: 300,
        maxHeight: 200,
      },
});

export default styles