import { View, useWindowDimensions, ScrollView, Image, SafeAreaView } from 'react-native'
import React from 'react'

import { useForm } from 'react-hook-form';

import InputField from '../../components/InputField';
import GeneralButtons from '../../components/GeneralButtons';
import { useNavigation } from '@react-navigation/native';

import Logo from '../../../assets/StematelLogo.png';
import styles from './styles';

export default function LoginScreen() {
  const { height } = useWindowDimensions();
  const navigation = useNavigation();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onbtnSignInPressed = data => {
    console.warn(data);

    navigation.navigate('HomeScreen')
  }
  return (
      <ScrollView showsVerticalScrollIndicator={false} style={{ backgroundColor: '#fff' }}>
        <View style={styles.root}>
          <Image
            source={Logo}
            style={[styles.logo, { height: height * 0.3 }]}
            resizeMode='contain'
          />

          <InputField
            name="username"
            placeholder="Username"
            control={control}
            rules={{ required: 'Username is required' }}
          />
          <InputField
            name="password"
            placeholder="Password"
            secureTextEntry
            control={control}
            rules={{
              required: 'Password is required',
              minLength: {
                value: 8,
                message: 'Password should be minimum 8 characters long',
              }
            }}
          />

          <GeneralButtons
            text="Sign in"
            isSingle={true}
            primary={true}
            onPress={handleSubmit(onbtnSignInPressed)}
          />
        </View>
      </ScrollView>
  )
}